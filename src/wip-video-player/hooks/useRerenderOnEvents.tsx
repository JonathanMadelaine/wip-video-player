import { useEffect, useState } from 'react'
import { VideoApi, VideoEvent } from 'wip-video-player/wipVideoApi'

export const useRerenderOnEvents = (
  videoApi: VideoApi | undefined,
  eventTypes: VideoEvent['type'][]
) => {
  const [, setState] = useState(0)

  useEffect(() => {
    let isMounted = true
    const subscriber = videoApi?.subscribe(events => {
      if (isMounted && events.some(e => eventTypes.includes(e.type))) {
        setState(p => p + 1)
      }
    })
    return () => {
      isMounted = false
      if (subscriber) {
        videoApi?.unsubscribe(subscriber)
      }
    }
  }, [videoApi, eventTypes])
}
