import { useEffect, useRef, useState } from 'react'
import { createVideoPlayer, VideoApi } from 'wip-video-player/wipVideoApi'

export const useVideoApi = (options: Parameters<typeof createVideoPlayer>[1]) => {
  const parentElementRef = useRef<HTMLDivElement | null>(null)
  const [videoApi, setVideoApi] = useState<VideoApi>()

  useEffect(() => {
    if (parentElementRef.current && (!videoApi || videoApi.getIsDisposed())) {
      setVideoApi(
        createVideoPlayer(parentElementRef.current, {
          src: options?.src,
        })
      )
    }
    return () => {
      if (videoApi) {
        videoApi.dispose()
      }
    }
  }, [options?.src, videoApi])

  return {
    parentElementRef,
    videoApi,
  }
}
