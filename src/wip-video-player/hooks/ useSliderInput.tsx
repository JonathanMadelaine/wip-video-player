import React, { useEffect, useRef } from 'react'

export const useSliderInput = (
  elRef: React.MutableRefObject<HTMLDivElement | null>,
  onPositionSet: (pc: number) => void,
  onPositionSetEnd?: () => void
) => {
  const isDownRef = useRef(false)
  const downPosRef = useRef(0)

  useEffect(() => {
    const el = elRef.current

    const onDown = (e: { clientX: number }) => {
      downPosRef.current = e.clientX
      if (el) {
        const rect = el.getBoundingClientRect()
        onPositionSet(Math.max(0, Math.min(1, (downPosRef.current - rect.x) / rect.width)))
      }
      isDownRef.current = true
    }

    const onMove = (e: { clientX: number }) => {
      if (el && isDownRef.current) {
        downPosRef.current = e.clientX
        const rect = el.getBoundingClientRect()
        onPositionSet(Math.max(0, Math.min(1, (downPosRef.current - rect.x) / rect.width)))
      }
    }

    const onUp = () => {
      if (isDownRef.current) {
        if (el && onPositionSetEnd) {
          onPositionSetEnd()
        }
        isDownRef.current = false
      }
    }

    if (el) {
      el.addEventListener('mousedown', onDown)
      window.addEventListener('mousemove', onMove)
      window.addEventListener('mouseup', onUp)
    }

    return () => {
      if (el) {
        el.removeEventListener('mousedown', onDown)
        window.removeEventListener('mousemove', onMove)
        window.removeEventListener('mouseup', onUp)
      }
    }
  }, [onPositionSet, onPositionSetEnd, elRef])
}
