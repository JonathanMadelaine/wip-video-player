import { useEffect } from 'react'
import { VideoApi } from 'wip-video-player/wipVideoApi'

export const useSyncMuteWithVolume = (videoApi: VideoApi | undefined) => {
  useEffect(() => {
    const subscriber = videoApi?.subscribe(events => {
      const volumeEvent = events.find(e => e.type === 'volume')
      // If setting volume to 0 when not muted, mute
      if (volumeEvent) {
        if (volumeEvent.value === 0) {
          if (!videoApi.getIsMuted()) {
            videoApi.mute()
          }
          // If setting volume to something else when muted, unmute
        } else if (videoApi.getIsMuted()) {
          videoApi.unmute()
        }
      }

      const isMutedEvent = events.find(e => e.type === 'isMuted')
      if (isMutedEvent) {
        // If muting and volume is not zero, set volume to 0
        if (isMutedEvent.value) {
          if (videoApi.getVolume() !== 0) {
            videoApi.setVolume(0)
          }
        }
        // If unmuting and volume is 0, set volume to 1
        else if (videoApi.getVolume() === 0) {
          videoApi.setVolume(1)
        }
      }
    })

    return () => {
      if (videoApi && subscriber) {
        videoApi.unsubscribe(subscriber)
      }
    }
  }, [videoApi])
}
