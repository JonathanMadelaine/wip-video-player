import { useCallback, useRef } from 'react'
import { VideoApi } from 'wip-video-player/wipVideoApi'

export const usePauseAndResumePlayMode = (videoApi: VideoApi | undefined) => {
  const wasPlayingPriorToPausing = useRef<boolean>()
  const pause = useCallback(() => {
    if (
      videoApi &&
      // If undefined, must be first skip action, so pause video until frame skip ended
      wasPlayingPriorToPausing.current === undefined
    ) {
      wasPlayingPriorToPausing.current = videoApi.getIsPlaying()
      videoApi.pause()
    }
  }, [videoApi])

  const resume = useCallback(() => {
    if (videoApi && wasPlayingPriorToPausing.current !== undefined) {
      if (wasPlayingPriorToPausing.current) {
        videoApi.play()
      }
      wasPlayingPriorToPausing.current = undefined
    }
  }, [videoApi])

  return {
    pause,
    resume,
  }
}
