import { useEffect, useRef } from 'react'

const isTargetTheBodyElement = (target: EventTarget) => target === document.body
const isNodeAnElement = (node: Node) => node.nodeType === Node.ELEMENT_NODE
const isElementAnInput = (element: Element) => element.nodeName === 'INPUT'
const isElementDescendentOfContainer = (element: Element | null, container: Element) => {
  while (element) {
    if (element.parentElement === container) {
      return true
    }
    element = element.parentElement
  }
  return false
}

const shouldFireHotkey = (container: HTMLElement | null, target: EventTarget | null) => {
  if (target === null || container === null) {
    return false
  }
  // Fire if 'nothing' is focussed (event target is body if nothing is focussed)
  if (isTargetTheBodyElement(target)) {
    return true
  }
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  let el: Element | null = target as Element
  // Don't fire if target is not an element, or if target is an input
  if (!isNodeAnElement(el) || isElementAnInput(el)) {
    return false
  }

  // Fire if target is descendent of container element
  return isElementDescendentOfContainer(el, container)
}

export const useHotkeys = ({
  container,
  keys,
}: {
  container: HTMLElement | null
  keys: Record<string, (() => void) | { down?: () => void; up?: () => void }>
}) => {
  /**
   * Hotkey definitions are in a deeply nested object. Passing this object to the main
   * useEffect's dep array (second useEffect below) means the effect runs every time the hook
   * is called. To prevent this, we could check deep equality of the hotkey definition object,
   * but an easy way is simply to store the hotkeys in a local ref object, and use this simple
   * useEffect to update hotkey definitions every time the hook is called. The new hotkey
   * definitions replace the old ones in the ref, preventing the main useEffect from rerunning
   * as its deps don't change, but allows hotkey definitions to be updated on re-render. Nice ;)
   */
  const hotkeyCache = useRef<typeof keys>(keys)
  useEffect(() => {
    hotkeyCache.current = keys
  }, [keys])

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (
        !Object.keys(hotkeyCache.current).includes(e.key) ||
        !shouldFireHotkey(container, e.target)
      ) {
        return
      }
      const hotkey = hotkeyCache.current[e.key]
      if (typeof hotkey === 'function') {
        e.preventDefault()
        hotkey()
      } else if (hotkey.down) {
        e.preventDefault()
        hotkey.down()
      }
    }
    const handleKeyUp = (e: KeyboardEvent) => {
      if (
        !Object.keys(hotkeyCache.current).includes(e.key) ||
        !shouldFireHotkey(container, e.target)
      ) {
        return
      }
      const hotkey = hotkeyCache.current[e.key]
      if (typeof hotkey !== 'function' && hotkey.up) {
        e.preventDefault()
        hotkey.up()
      }
    }
    window.addEventListener('keydown', handleKeyDown)
    window.addEventListener('keyup', handleKeyUp)
    return () => {
      window.removeEventListener('keydown', handleKeyDown)
      window.removeEventListener('keyup', handleKeyUp)
    }
  }, [container])
}
