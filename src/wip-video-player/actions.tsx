import {
  floorSecondsToNearestFrame,
  framesToSeconds,
  secondsToWholeFrames,
  timecodeToSeconds,
} from './utils'
import { VideoApi } from './wipVideoApi'

export const togglePlay = (videoApi: VideoApi | undefined, frameRate: number) => {
  if (videoApi) {
    if (videoApi.getIsPlaying()) {
      videoApi.pause()
      // If we are pausing the video, set to start of current frame
      videoApi.setCurrentTime(
        floorSecondsToNearestFrame(videoApi.getCurrentTime(), frameRate) +
          // Add a couple of frames to prevent video jumping backwards when paused
          framesToSeconds(2, frameRate)
      )
    } else {
      videoApi.play()
    }
  }
}

export const toggleLoop = (videoApi: VideoApi | undefined) => {
  if (videoApi) {
    videoApi.getIsLooped() ? videoApi.unloop() : videoApi.loop()
  }
}

export const toggleMute = (videoApi: VideoApi | undefined) => {
  if (videoApi) {
    videoApi.getIsMuted() ? videoApi.unmute() : videoApi.mute()
  }
}

export const frameSkip = (
  videoApi: VideoApi | undefined,
  frameRate: number,
  frameDelta: number
) => {
  if (videoApi) {
    const currentF = secondsToWholeFrames(videoApi.getCurrentTime(), frameRate)
    const newT = framesToSeconds(currentF + frameDelta, frameRate)
    // Loss of accurracy during floating point calculations means somtimes new time is calculated as being within the previous-but-one frame.
    // Time buffer pushes time into previous frame.
    const timeBuffer = 0.00001
    videoApi.setCurrentTime(newT + timeBuffer)
  }
}

export const cueToTimecode = (videoApi: VideoApi | undefined, frameRate: number, time: string) => {
  if (videoApi) {
    const s = timecodeToSeconds(time, frameRate)
    if (s !== undefined) {
      // Bump forward into the correct frame if not 0
      const timeBuffer = 0.00001
      videoApi.setCurrentTime(s === 0 ? s : s + timeBuffer)
    }
    // Return boolean success or failed to parse
    return !!s
  }
}

export const seek = (videoApi: VideoApi | undefined, frameRate: number, percent: number) => {
  if (videoApi) {
    videoApi.setCurrentTime(
      floorSecondsToNearestFrame((videoApi.getDuration() ?? 0) * percent, frameRate)
    )
  }
}
