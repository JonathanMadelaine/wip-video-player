import Hls from 'hls.js'

/*
 * Types
 */

export type VideoEvent =
  | {
      type: 'bufferedTimeRanges'
      value: [start: number, end: number][]
    }
  | {
      type: 'duration'
      value: number | undefined
    }
  | {
      type: 'isPlaying'
      value: boolean
    }
  | {
      type: 'isReady'
      value: boolean
    }
  | {
      type: 'isLooped'
      value: boolean
    }
  | {
      type: 'isMuted'
      value: boolean
    }
  | {
      type: 'src'
      value: string
    }
  | {
      type: 'currentTime'
      value: number
    }
  | {
      type: 'playbackRate'
      value: number
    }
  | {
      type: 'volume'
      value: number
    }
  | {
      type: 'src'
      value: string
    }

type SubscriberCallback = (events: VideoEvent[]) => void

type Options = {
  src?: string
}

export type VideoApi = {
  dispose: () => void
  enterFullscreen: () => Promise<void>
  getBufferedTimeRanges: () => [start: number, end: number][]
  getCurrentTime: () => number
  setCurrentTime: (currentTime: number) => void
  getDuration: () => number | undefined
  getIsDisposed: () => boolean
  getIsReady: () => boolean
  getIsLooped: () => boolean
  getIsMuted: () => boolean
  getIsPlaying: () => boolean
  getParentElement: () => HTMLElement
  getPlaybackRate: () => number
  getSrc: () => string
  getVolume: () => number
  setVolume: (volume: number) => void
  loop: () => void
  unloop: () => void
  mute: () => void
  unmute: () => void
  play: () => void
  pause: () => void
  setPlaybackRate: (rate: number) => void
  setSrc: (src: string) => void
  subscribe: (callback: SubscriberCallback) => number
  unsubscribe: (id: number) => void
}

/*
 * Src helpers
 */

const srcIsHlsUri = (src: string) => src.endsWith('.m3u8')
const hlsIsSupportedViaMse = () => Hls.isSupported()
const setSrcAsHlsWithMse = (video: HTMLVideoElement, src: string) => {
  const hls = new Hls()
  hls.loadSource(src)
  hls.attachMedia(video)
}
const hlsIsSupportedNatively = (video: HTMLVideoElement) =>
  video.canPlayType('application/vnd.apple.mpegurl')
const setSrcNormally = (video: HTMLVideoElement, src: string) => (video.src = src)
const handleSetSrc = (video: HTMLVideoElement, src: string) => {
  if (srcIsHlsUri(src)) {
    if (hlsIsSupportedViaMse()) {
      setSrcAsHlsWithMse(video, src)
    } else if (hlsIsSupportedNatively(video)) {
      setSrcNormally(video, src)
    } else {
      // TODO: error, HLS unsupported
    }
  } else {
    setSrcNormally(video, src)
  }
}

/*
 * Event subscriber helpers
 */

const alertSubscribers = (
  subscribers: Record<number, SubscriberCallback>,
  events: VideoEvent[]
) => {
  const callbacks = Object.values(subscribers)
  for (const callback of callbacks) {
    callback(events)
  }
}

/*
 * Video API builder
 */

export const createVideoPlayer = (parentElement: HTMLElement, options?: Options): VideoApi => {
  const parent = parentElement
  let video = document.createElement('video')
  const subscribers: Record<number, SubscriberCallback> = {}
  let isReady = false
  let eventLoop: ReturnType<typeof requestAnimationFrame> | undefined
  let isDisposed = false

  /*
   * Style
   */

  video.style.display = 'block' // removes 4px gap at bottom
  video.style.blockSize = '100%' // allows video to resize
  video.style.inlineSize = '100%' // allows video to resize

  /*
   * Set src
   */

  handleSetSrc(video, options?.src || '')

  /*
   * Define video API
   */

  const dispose = () => {
    // Clear the event loop
    if (eventLoop !== undefined) {
      cancelAnimationFrame(eventLoop)
    }

    // Remove video element from DOM
    if (Array.from(parent.childNodes).includes(video)) {
      parent.removeChild(video)
    }

    // Unload media from video element
    video.pause()
    video.removeAttribute('src')
    video.load()

    isDisposed = true
  }

  const getIsDisposed = () => isDisposed

  const getParentElement = () => parent

  const getSrc = () => video.src
  const setSrc = (src: string) => {
    handleSetSrc(video, src)
  }

  const getCurrentTime = () => video.currentTime
  const setCurrentTime = (currentTime: number) => {
    video.currentTime = Math.max(0, Math.min(video.duration, currentTime))
  }

  const getDuration = () => {
    const duration = video.duration
    return isNaN(duration) || duration === Infinity ? undefined : duration
  }

  const getBufferedTimeRanges = () => {
    const buffered = video.buffered
    let timeRanges: [start: number, end: number][] = []
    for (let i = 0; i < buffered.length; ++i) {
      timeRanges.push([buffered.start(i), buffered.end(i)])
    }
    return timeRanges
  }

  const getPlaybackRate = () => video.playbackRate
  const setPlaybackRate = (rate: number) => {
    // playback rate only supports values of 0.0625 to 16, inclusive
    video.playbackRate = Math.max(0.0625, Math.min(rate, 16))
  }

  const getVolume = () => video.volume
  const setVolume = (volume: number) => {
    const v = Math.max(0, Math.min(volume, 1))
    video.volume = v
  }

  const getIsPlaying = () => !video.paused
  const play = () => {
    if (isReady) {
      video.play().catch(() => {
        // Deliberately swallows error "The play() request was interrupted by a call to pause()"
      })
    }
  }
  const pause = () => {
    if (isReady) {
      video.pause()
    }
  }

  const getIsLooped = () => video.loop
  const loop = () => {
    video.loop = true
  }
  const unloop = () => {
    video.loop = false
  }

  const getIsMuted = () => video.muted
  const mute = () => {
    video.muted = true
  }
  const unmute = () => {
    video.muted = false
  }

  const getIsReady = () => isReady

  const enterFullscreen = async () => video.requestFullscreen()

  // Subscribe to state changes
  const subscribe = (callback: SubscriberCallback) => {
    const subscriberIds = Object.keys(subscribers)
    let id = subscriberIds.length
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    while (subscribers[id]) {
      id++
    }
    subscribers[id] = callback
    return id
  }

  const unsubscribe = (id: number) => {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (subscribers[id]) {
      // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
      delete subscribers[id]
    }
  }

  /*
   * Custom events
   */

  const eventValueCache = {
    bufferedTimeRanges: getBufferedTimeRanges(),
    currentTime: getCurrentTime(),
    duration: getDuration(),
    isLooped: getIsLooped(),
    isMuted: getIsMuted(),
    isPlaying: getIsPlaying(),
    isReady: getIsReady(),
    playbackRate: getPlaybackRate(),
    src: getSrc(),
    volume: getVolume(),
  }

  const eventUpdate = () => {
    const events: VideoEvent[] = []

    const currentBufferedTimeRanges = getBufferedTimeRanges()
    let bufferedTimeRangesChanged =
      eventValueCache.bufferedTimeRanges.length !== currentBufferedTimeRanges.length
    if (!bufferedTimeRangesChanged) {
      for (let i = 0; i < currentBufferedTimeRanges.length; ++i) {
        if (
          eventValueCache.bufferedTimeRanges[i][0] !== currentBufferedTimeRanges[i][0] ||
          eventValueCache.bufferedTimeRanges[i][1] !== currentBufferedTimeRanges[i][1]
        ) {
          bufferedTimeRangesChanged = true
          break
        }
      }
    }
    if (bufferedTimeRangesChanged) {
      eventValueCache.bufferedTimeRanges = currentBufferedTimeRanges
      events.push({
        type: 'bufferedTimeRanges',
        value: currentBufferedTimeRanges,
      })
    }

    const currentDuration = getDuration()
    if (eventValueCache.duration !== currentDuration) {
      eventValueCache.duration = currentDuration
      events.push({
        type: 'duration',
        value: currentDuration,
      })
    }

    const currentIsPlaying = getIsPlaying()
    if (eventValueCache.isPlaying !== currentIsPlaying) {
      eventValueCache.isPlaying = currentIsPlaying
      events.push({
        type: 'isPlaying',
        value: currentIsPlaying,
      })
    }

    const currentIsReady = getIsReady()
    if (eventValueCache.isReady !== currentIsReady) {
      eventValueCache.isReady = currentIsReady
      events.push({
        type: 'isReady',
        value: currentIsReady,
      })
    }

    const currentIsLooped = getIsLooped()
    if (eventValueCache.isLooped !== currentIsLooped) {
      eventValueCache.isLooped = currentIsLooped
      events.push({
        type: 'isLooped',
        value: currentIsLooped,
      })
    }

    const currentIsMuted = getIsMuted()
    if (eventValueCache.isMuted !== currentIsMuted) {
      eventValueCache.isMuted = currentIsMuted
      events.push({
        type: 'isMuted',
        value: currentIsMuted,
      })
    }

    const currentSrc = getSrc()
    if (eventValueCache.src !== currentSrc) {
      eventValueCache.src = currentSrc
      events.push({
        type: 'src',
        value: currentSrc,
      })
    }

    const currentCurrentTime = getCurrentTime()
    if (eventValueCache.currentTime !== currentCurrentTime) {
      eventValueCache.currentTime = currentCurrentTime
      events.push({
        type: 'currentTime',
        value: currentCurrentTime,
      })
    }

    const currentPlaybackRate = getPlaybackRate()
    if (eventValueCache.playbackRate !== currentPlaybackRate) {
      eventValueCache.playbackRate = currentPlaybackRate
      events.push({
        type: 'playbackRate',
        value: currentPlaybackRate,
      })
    }

    const currentVolume = getVolume()
    if (eventValueCache.volume !== currentVolume) {
      eventValueCache.volume = currentVolume
      events.push({
        type: 'volume',
        value: currentVolume,
      })
    }

    if (events.length) {
      alertSubscribers(subscribers, events)
    }

    eventLoop = requestAnimationFrame(eventUpdate)
  }

  eventLoop = requestAnimationFrame(eventUpdate)

  /*
   * Native events
   */

  // Waiting for video to buffer
  video.onwaiting = () => {
    isReady = false
  }
  video.oncanplay = () => {
    isReady = true
  }

  /*
   * Render video element
   */

  parent.innerHTML = '' // clear existing children if any
  parent.appendChild(video)

  /*
   * Return video API
   */

  return {
    dispose,
    enterFullscreen,
    getBufferedTimeRanges,
    getCurrentTime,
    getDuration,
    getIsDisposed,
    getIsReady,
    getIsLooped,
    getIsMuted,
    getIsPlaying,
    getParentElement,
    getPlaybackRate,
    getSrc,
    getVolume,
    setCurrentTime,
    play,
    pause,
    loop,
    unloop,
    mute,
    unmute,
    setPlaybackRate,
    setSrc,
    setVolume,
    subscribe,
    unsubscribe,
  }
}
