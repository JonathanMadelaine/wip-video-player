type TimeFormat = 'timecode' | 'frames' | 'time'

export const formatTime = (
  { timeInSeconds, frameRate }: { timeInSeconds: number | undefined; frameRate: number },
  format: TimeFormat
) => {
  switch (format) {
    case 'time': {
      if (timeInSeconds === undefined) {
        return `--:--:--.---`
      }
      let total = timeInSeconds
      const h = secondsToWholeHours(total)
      total -= h * 3600
      const m = secondsToWholeMinutes(total)
      total -= m * 60
      const s = secondsToWholeSeconds(total)
      total -= s
      const ms = secondsToWholeMilliseconds(total)
      return `${`${h}`.padStart(2, '0')}:${`${m}`.padStart(2, '0')}:${`${s}`.padStart(
        2,
        '0'
      )}.${`${ms}`.padStart(3, '0')}`
    }
    // Timecode non-dropframe (NDF)
    case 'timecode': {
      if (timeInSeconds === undefined) {
        return `--:--:--:--`
      }
      // Calculate timecode from total frame count instead of total time, as we are using non-dropframe timecode
      let fr = secondsToWholeFrames(timeInSeconds, frameRate)
      // Round frame rate to allow for non-dropframe timecode generation. We pretend frame rate is whole number, e.g. 29.97fps => 30fps
      const roundedFrameRate = Math.round(frameRate)

      const framesInAnHour = roundedFrameRate * 3600
      const h = Math.floor(fr / framesInAnHour)
      fr -= framesInAnHour * h

      const framesInAMinute = roundedFrameRate * 60
      const m = Math.floor(fr / framesInAMinute)
      fr -= framesInAMinute * m

      const s = Math.floor(fr / roundedFrameRate)
      fr -= roundedFrameRate * s

      return `${`${h}`.padStart(2, '0')}:${`${m}`.padStart(2, '0')}:${`${s}`.padStart(
        2,
        '0'
      )}:${`${fr}`.padStart(2, '0')}`
    }
    // TODO: allow dropframe timecodes (DF) for supported formats: 29.97, 59.94

    case 'frames': {
      if (timeInSeconds === undefined) {
        return `--`
      }
      const fr = secondsToWholeFrames(timeInSeconds, frameRate)
      return `${fr}`
    }
  }
}

const secondsToWholeHours = (timeInSeconds: number) => Math.floor(timeInSeconds / 3600)

const secondsToWholeMinutes = (timeInSeconds: number) => Math.floor(timeInSeconds / 60)

const secondsToWholeSeconds = (timeInSeconds: number) => Math.floor(timeInSeconds)

const secondsToWholeMilliseconds = (timeInSeconds: number) => Math.floor(timeInSeconds * 1000)

export const secondsToWholeFrames = (timeInSeconds: number, frameRate: number) =>
  Math.floor(timeInSeconds * frameRate)

export const framesToSeconds = (frames: number, frameRate: number) => {
  const f = Math.max(0, Math.floor(frames))
  const frameDuration = 1 / (frameRate || 1)
  return f * frameDuration
}

export const floorSecondsToNearestFrame = (timeInSeconds: number, frameRate: number) => {
  const durationOfSingleFrameinSeconds = 1 / frameRate
  return Math.floor(timeInSeconds / durationOfSingleFrameinSeconds) * durationOfSingleFrameinSeconds
}

export const timecodeToSeconds = (timecode: string, frameRate: number) => {
  const tc = /^(\d+):(\d+):(\d+):(\d+)$/.exec(timecode.trim())

  if (tc?.length !== 5) {
    return undefined
  }

  // timecode is non-dropframe (rounded frame rate), so calcuate total frames defined in timecode, and convert to actual time
  const roundedFrameRate = Math.round(frameRate)
  const timecodeFrameCount =
    Number(tc[1]) * roundedFrameRate * 3600 + // frames in timecode hours
    Number(tc[2]) * roundedFrameRate * 60 + // frames in timecode minutes
    Number(tc[3]) * roundedFrameRate + // frames in timecode seconds
    Number(tc[4])

  // Use actual frame rate to get real time for selected frame of timecode
  return timecodeFrameCount * (1 / frameRate)
}
