/** @jsxImportSource @emotion/react */

import { Block } from 'baselift'
import React, { FC, useRef } from 'react'
import { seek } from 'wip-video-player/actions'
import { useSliderInput } from 'wip-video-player/hooks/ useSliderInput'
import { usePauseAndResumePlayMode } from 'wip-video-player/hooks/usePauseAndResumePlayMode'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'
import { floorSecondsToNearestFrame } from 'wip-video-player/utils'
import { VideoApi } from '../wipVideoApi'

type SeekBarProps = {
  videoApi: VideoApi
  frameRate: number
}

export const SeekBar: FC<SeekBarProps> = ({ videoApi, frameRate }) => {
  const elRef = useRef<HTMLDivElement | null>(null)

  const pauseAndResumePlayMode = usePauseAndResumePlayMode(videoApi)

  useSliderInput(
    elRef,
    pc => {
      pauseAndResumePlayMode.pause()
      seek(videoApi, frameRate, pc)
    },
    pauseAndResumePlayMode.resume
  )
  useRerenderOnEvents(videoApi, ['duration', 'currentTime', 'bufferedTimeRanges'])

  const duration = videoApi.getDuration()
  const progressPercent = duration
    ? (floorSecondsToNearestFrame(videoApi.getCurrentTime(), frameRate) / duration) * 100
    : 0

  return (
    <Block
      ref={elRef}
      css={{
        blockSize: 'var(--wvp-seek-height)',
        backgroundColor: 'var(--wvp-seek-background)',
        cursor: 'pointer',
      }}
    >
      {videoApi.getBufferedTimeRanges().map(tr => (
        <Block
          key={`${tr[0]}-${tr[1]}`}
          css={{
            position: 'absolute',
            pointerEvents: 'none',
            top: 0,
            left: `${(duration ? tr[0] / duration : 0) * 100}%`,
            bottom: 0,
            right: `${100 - (duration ? tr[1] / duration : 0) * 100}%`,
            backgroundColor: 'var(--wvp-seek-buffered-color)',
          }}
        />
      ))}
      <Block
        css={{
          position: 'absolute',
          pointerEvents: 'none',
          top: 0,
          left: 0,
          bottom: 0,
          right: `${100 - progressPercent}%`,
          backgroundColor: 'var(--wvp-seek-color)',
        }}
      />
    </Block>
  )
}
