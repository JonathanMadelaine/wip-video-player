/** @jsxImportSource @emotion/react */

import React, { FC, ReactNode, useRef } from 'react'
import { SeekBar } from './SeekBar'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'
import { ControlBar } from './control_bar/ControlBar'
import { Block } from 'baselift'
import { useSyncMuteWithVolume } from 'wip-video-player/hooks/useSyncMuteWithVolume'
import { useVideoApi } from 'wip-video-player/hooks/useVideoApi'
import { useHotkeys } from 'wip-video-player/hooks/useHotkeys'
import { frameSkip, toggleLoop, toggleMute, togglePlay } from 'wip-video-player/actions'
import { usePauseAndResumePlayMode } from 'wip-video-player/hooks/usePauseAndResumePlayMode'

interface PlayerProps {
  media: {
    frameRate: number
    src: string
  }
  loadingOverlay?: ReactNode
}

export const Player: FC<PlayerProps> = ({ media, loadingOverlay, ...props }) => {
  const { frameRate, src } = media

  const containerRef = useRef<HTMLDivElement | null>(null)
  const { videoApi, parentElementRef: videoParentRef } = useVideoApi({ src })

  useRerenderOnEvents(videoApi, ['isReady'])
  useSyncMuteWithVolume(videoApi)

  const pauseAndResumePlayMode = usePauseAndResumePlayMode(videoApi)

  useHotkeys({
    container: containerRef.current,
    keys: {
      ' ': () => togglePlay(videoApi, frameRate),
      m: () => toggleMute(videoApi),
      l: () => toggleLoop(videoApi),
      // c: // TODO add cueToTime hotkey
      ArrowLeft: {
        down: () => {
          pauseAndResumePlayMode.pause()
          frameSkip(videoApi, frameRate, -1)
        },
        up: pauseAndResumePlayMode.resume,
      },
      ArrowRight: {
        down: () => {
          pauseAndResumePlayMode.pause()
          frameSkip(videoApi, frameRate, 1)
        },
        up: pauseAndResumePlayMode.resume,
      },
    },
  })

  return (
    <Block ref={containerRef} {...props}>
      <Block>
        <div ref={videoParentRef} />
        {!videoApi?.getIsReady() && loadingOverlay}
      </Block>
      {videoApi ? (
        <>
          <SeekBar videoApi={videoApi} frameRate={frameRate} />
          <ControlBar videoApi={videoApi} frameRate={frameRate} />
        </>
      ) : null}
    </Block>
  )
}
