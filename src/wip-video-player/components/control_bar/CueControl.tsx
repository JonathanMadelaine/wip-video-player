/** @jsxImportSource @emotion/react */

import { CueIcon } from '../icons'
import React, { FC, useEffect, useRef, useState } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { Flex, TextInput } from 'baselift'
import { ControlBarButton } from './ControlBarButton'
import { cueToTimecode } from 'wip-video-player/actions'

interface CueControlProps {
  videoApi: VideoApi
  frameRate: number
}

export const CueControl: FC<CueControlProps> = ({ videoApi, frameRate }) => {
  const [value, setValue] = useState('00:00:00:00')
  const [cursorPosition, setCursorPosition] = useState({ pos: 0, renderBuster: true })

  const textboxRef = useRef<HTMLInputElement | null>(null)

  useEffect(() => {
    if (textboxRef.current) {
      textboxRef.current.selectionStart = cursorPosition.pos
      textboxRef.current.selectionEnd = cursorPosition.pos
    }
  })

  return (
    <Flex
      css={{
        blockSize: '100%',
      }}
    >
      <ControlBarButton
        onClick={() => {
          const successfulCue = cueToTimecode(videoApi, frameRate, value)
          if (!successfulCue) {
            // TODO: highlight textbox as error, failed to parse timecode
          }
        }}
      >
        <CueIcon />
      </ControlBarButton>
      <TextInput
        ref={textboxRef}
        value={value}
        onChange={e => {
          let val = e.target.value.replace(/[^0-9:]/g, '')
          let cursorPos = e.target.selectionStart ?? 0

          if (val === value.replace(/[^0-9:]/g, '')) {
            setCursorPosition(prev => {
              return { ...prev, renderBuster: !prev.renderBuster }
            })
            return
          }

          const clean = (s: string) => s.replace(/[^0-9]/g, '')

          setValue(prev => {
            let firstChangedCharIndex = 0
            for (
              firstChangedCharIndex;
              firstChangedCharIndex < val.length;
              ++firstChangedCharIndex
            ) {
              if (val[firstChangedCharIndex] !== prev[firstChangedCharIndex]) {
                break
              }
            }

            const isDeleting = val.length < prev.length && cursorPos - 1 < firstChangedCharIndex

            const colonIndexes = [2, 5, 8]
            if (isDeleting && colonIndexes.includes(firstChangedCharIndex)) {
              val = `${val.slice(0, firstChangedCharIndex - 1)}${val.slice(firstChangedCharIndex)}`
              firstChangedCharIndex--
            }

            const cleanVal = clean(val)
            const cleanPrev = clean(prev)

            const digitPrefix = cleanVal.slice(
              0,
              firstChangedCharIndex - Math.floor(firstChangedCharIndex / 3)
            )

            const enteredDigits = clean(val.slice(firstChangedCharIndex, cursorPos))

            let newValueDigits = [...cleanPrev]

            if (isDeleting) {
              const deletedCharCount = cleanPrev.length - cleanVal.length
              for (let i = 0; i < deletedCharCount; ++i) {
                const valIndex = i + digitPrefix.length
                newValueDigits[valIndex] = '0'
              }
            } else {
              for (let i = 0; i < enteredDigits.length; ++i) {
                const valIndex = i + digitPrefix.length
                newValueDigits[valIndex] = enteredDigits[i]
              }
            }

            setCursorPosition(p => {
              const pos =
                isDeleting && colonIndexes.includes(cursorPos)
                  ? cursorPos - 1
                  : !isDeleting &&
                    (colonIndexes.includes(cursorPos) || colonIndexes.includes(cursorPos - 1))
                  ? cursorPos + 1
                  : cursorPos
              return { pos, renderBuster: !p.renderBuster }
            })

            return newValueDigits
              .slice(0, 8)
              .map((v, i) => (i === 2 || i === 4 || i === 6 ? `:${v}` : v))
              .join('')
          })
        }}
        css={{
          backgroundColor: 'var(--wvp-background-shade)',
          inlineSize: '120px',
          textAlign: 'center',
          color: 'var(--wvp-color-shade)',
          '&:focus': {
            color: 'var(--wvp-color)',
          },
        }}
      />
    </Flex>
  )
}
