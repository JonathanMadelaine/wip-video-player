/** @jsxImportSource @emotion/react */

import { FullscreenIcon } from '../icons'
import React, { FC } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { ControlBarButton } from './ControlBarButton'

interface FullscreenControlProps {
  videoApi: VideoApi
}

export const FullscreenControl: FC<FullscreenControlProps> = ({ videoApi }) => {
  return (
    <ControlBarButton
      onClick={() => {
        videoApi.enterFullscreen()
      }}
    >
      <FullscreenIcon />
    </ControlBarButton>
  )
}
