/** @jsxImportSource @emotion/react */

import { PauseIcon, PlayIcon } from '../icons'
import React, { FC } from 'react'
import { ControlBarButton } from './ControlBarButton'
import { togglePlay } from 'wip-video-player/actions'
import { VideoApi } from 'wip-video-player/wipVideoApi'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'

interface PlayControlProps {
  videoApi: VideoApi
  frameRate: number
}

export const PlayControl: FC<PlayControlProps> = ({ videoApi, frameRate }) => {
  useRerenderOnEvents(videoApi, ['isPlaying'])

  return (
    <ControlBarButton
      onClick={() => {
        togglePlay(videoApi, frameRate)
      }}
    >
      {videoApi.getIsPlaying() ? <PauseIcon /> : <PlayIcon />}
    </ControlBarButton>
  )
}
