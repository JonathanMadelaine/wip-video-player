/** @jsxImportSource @emotion/react */

import { VolumeDownIcon, VolumeMuteIcon, VolumeUpIcon } from '../icons'
import { FC, useRef } from 'react'
import { Block, Flex } from 'baselift'
import { useSliderInput } from 'wip-video-player/hooks/ useSliderInput'
import { ControlBarButton } from './ControlBarButton'
import { toggleMute } from 'wip-video-player/actions'
import { VideoApi } from 'wip-video-player/wipVideoApi'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'

interface VolumeControlProps {
  videoApi: VideoApi
}

export const VolumeControl: FC<VolumeControlProps> = ({ videoApi }) => {
  useRerenderOnEvents(videoApi, ['volume', 'isMuted'])

  return (
    <Flex
      css={{
        blockSize: '100%',
        paddingRight: '8px',
      }}
    >
      <ControlBarButton
        onClick={() => {
          toggleMute(videoApi)
        }}
      >
        {videoApi.getIsMuted() ? (
          <VolumeMuteIcon />
        ) : videoApi.getVolume() < 0.5 ? (
          <VolumeDownIcon />
        ) : (
          <VolumeUpIcon />
        )}
      </ControlBarButton>
      <VolumeSlider volume={videoApi.getVolume()} setVolume={videoApi.setVolume} />
    </Flex>
  )
}

interface VolumeSliderProps {
  volume: number
  setVolume: (volume: number) => void
}

const VolumeSlider: FC<VolumeSliderProps> = ({ volume, setVolume }) => {
  const controlWidth = 64
  const circleRadius = 6
  // Padded left and right areas of slider
  const deadzoneWidth = 8

  const elRef = useRef<HTMLDivElement | null>(null)
  useSliderInput(elRef, pos => {
    setVolume(0.5 + ((pos - 0.5) * controlWidth) / (controlWidth - deadzoneWidth * 2))
  })

  return (
    <Block
      ref={elRef}
      css={{
        inlineSize: `${controlWidth}px`,
        blockSize: '100%',
        cursor: 'pointer',
      }}
    >
      <Block
        css={{
          pointerEvents: 'none',
          position: 'absolute',
          left: `${deadzoneWidth}px`,
          top: '50%',
          transform: 'translate(0, -50%)',
          height: '2px',
          right: `${deadzoneWidth}px`,
          backgroundColor: 'var(--wvp-color)',
        }}
      />
      <Block
        css={{
          pointerEvents: 'none',
          width: `${circleRadius * 2}px`,
          height: `${circleRadius * 2}px`,
          borderRadius: '50%',
          backgroundColor: 'var(--wvp-color)',
          position: 'absolute',
          top: '50%',
          transform: 'translate(0, -50%)',
          left: `${(controlWidth - deadzoneWidth * 2) * volume + deadzoneWidth - circleRadius}px`,
        }}
      />
    </Block>
  )
}
