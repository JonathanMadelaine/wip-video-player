/** @jsxImportSource @emotion/react */

import React, { FC } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { ControlBarButton } from './ControlBarButton'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'

interface PlaybackRateControlProps {
  videoApi: VideoApi
}

export const PlaybackRateControl: FC<PlaybackRateControlProps> = ({ videoApi }) => {
  useRerenderOnEvents(videoApi, ['playbackRate'])

  const playbackRates = [0.125, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 4, 8]

  // TODO: this is quick and dirty for testing, replace with a rate selector
  // tooltip / popup
  const onClick = () => {
    const pbR = videoApi.getPlaybackRate()
    if (pbR === 8) {
      videoApi.setPlaybackRate(0.125)
    } else {
      videoApi.setPlaybackRate(playbackRates[playbackRates.indexOf(pbR) + 1])
    }
  }

  return <ControlBarButton onClick={onClick}>{`${videoApi.getPlaybackRate()}x`}</ControlBarButton>
}
