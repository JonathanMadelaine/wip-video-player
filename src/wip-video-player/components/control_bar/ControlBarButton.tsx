/** @jsxImportSource @emotion/react */

import { Button } from 'baselift'
import { ComponentProps, forwardRef } from 'react'

export const ControlBarButton = forwardRef<HTMLButtonElement, ComponentProps<typeof Button>>(
  (props, ref) => {
    return (
      <Button
        ref={ref}
        onKeyDown={e => {
          // Space is used as hotkey, so disable it on buttons
          if (e.key === ' ') {
            e.preventDefault()
          }
        }}
        css={{
          blockSize: '100%',
          inlineSize: 'var(--wvp-controls-height)',
          padding: '8px 0',
          color: 'var(--wvp-color)',
        }}
        {...props}
      />
    )
  }
)
