/** @jsxImportSource @emotion/react */

import { NextArrowIcon, PrevArrowIcon } from '../icons'
import React, { FC, useEffect, useRef } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { ControlBarButton } from './ControlBarButton'
import { frameSkip } from 'wip-video-player/actions'
import { usePauseAndResumePlayMode } from 'wip-video-player/hooks/usePauseAndResumePlayMode'
import { Block } from 'baselift'

interface SkipFrameControlProps {
  videoApi: VideoApi
  frameRate: number
}

export const SkipFrameControl: FC<SkipFrameControlProps> = ({ videoApi, frameRate }) => {
  return (
    <Block
      css={{
        blockSize: '100%',
      }}
    >
      <SkipFrameButton videoApi={videoApi} frameRate={frameRate} direction="prev" />
      <SkipFrameButton videoApi={videoApi} frameRate={frameRate} direction="next" />
    </Block>
  )
}

interface SkipFrameButtonProps {
  videoApi: VideoApi
  frameRate: number
  direction: 'prev' | 'next'
}

const SkipFrameButton: FC<SkipFrameButtonProps> = ({
  videoApi,
  frameRate,
  direction,
  ...props
}) => {
  const ref = useRef<HTMLButtonElement | null>(null)

  const { pause, resume: resumePlayMode } = usePauseAndResumePlayMode(videoApi)

  useEffect(() => {
    const el = ref.current

    let interval: ReturnType<typeof setInterval> | undefined
    let timeout: ReturnType<typeof setTimeout> | undefined

    const start = () => {
      pause()
      const onDown = () => frameSkip(videoApi, frameRate, direction === 'prev' ? -1 : 1)
      onDown()
      // debounce further clicks
      timeout = setTimeout(() => {
        interval = setInterval(() => {
          onDown()
        }, 33)
      }, 250)
    }

    const end = () => {
      if (timeout) {
        clearTimeout(timeout)
      }
      if (interval) {
        clearInterval(interval)
      }
      resumePlayMode()
    }

    if (el) {
      el.addEventListener('mousedown', start)
      window.addEventListener('mouseup', end)
    }

    return () => {
      if (el) {
        el.removeEventListener('mousedown', start)
        window.removeEventListener('mouseup', end)
      }

      if (timeout) {
        clearTimeout(timeout)
      }
      if (interval) {
        clearInterval(interval)
      }
    }
  }, [direction, frameRate, videoApi, pause, resumePlayMode])

  return (
    <ControlBarButton ref={ref} {...props}>
      {direction === 'prev' ? <PrevArrowIcon /> : <NextArrowIcon />}
    </ControlBarButton>
  )
}
