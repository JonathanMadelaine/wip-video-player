/** @jsxImportSource @emotion/react */

import React, { FC } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { Block, Stack } from 'baselift'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'
import { VolumeControl } from './VolumeControl'
import { PlayControl } from './PlayControl'
import { TimeDisplayControl } from './TimeDisplayControl'
import { LoopControl } from './LoopControl'
import { CueControl } from './CueControl'
import { PlaybackRateControl } from './PlaybackRateControl'
import { SkipFrameControl } from './SkipFrameControl'
import { FullscreenControl } from './FullscreenControl'

type ControlBarProps = {
  videoApi: VideoApi
  frameRate: number
}

export const ControlBar: FC<ControlBarProps> = ({ videoApi, frameRate }) => {
  return (
    <Block
      css={{
        backgroundColor: 'var(--wvp-background)',
        color: 'var(--wvp-color)',
      }}
    >
      <SimpleControlBar videoApi={videoApi} frameRate={frameRate} />
      <ExtendedControlBar videoApi={videoApi} frameRate={frameRate} />
    </Block>
  )
}

type SimpleControlBarProps = {
  videoApi: VideoApi
  frameRate: number
}

const SimpleControlBar: FC<SimpleControlBarProps> = ({ videoApi, frameRate }) => {
  useRerenderOnEvents(videoApi, ['isPlaying', 'isMuted', 'volume'])

  return (
    <Stack
      css={{
        blockSize: 'var(--wvp-controls-height)',
        inlineSize: '100%',
      }}
    >
      <Stack
        css={{
          padding: '0 8px',
        }}
      >
        <PlayControl videoApi={videoApi} frameRate={frameRate} />
        <VolumeControl videoApi={videoApi} />
      </Stack>
      <Stack
        css={{
          flex: 1,
          blockSize: '100%',
          padding: '0 8px',
        }}
        inlineAlign="center"
        blockAlign="center"
      >
        <TimeDisplayControl videoApi={videoApi} frameRate={frameRate} />
      </Stack>
      <Stack
        css={{
          padding: '0 8px',
        }}
      >
        <FullscreenControl videoApi={videoApi} />
      </Stack>
    </Stack>
  )
}

type ExtendedControlBarProps = {
  videoApi: VideoApi
  frameRate: number
}

export const ExtendedControlBar: FC<ExtendedControlBarProps> = ({ videoApi, frameRate }) => (
  <Stack
    css={{
      blockSize: 'var(--wvp-controls-height)',
      inlineSize: '100%',
      borderTop: '1px solid var(--wvp-background-shade)',
      padding: '0 8px',
    }}
  >
    <LoopControl videoApi={videoApi} />
    <PlaybackRateControl videoApi={videoApi} />
    <CueControl videoApi={videoApi} frameRate={frameRate} />
    <SkipFrameControl videoApi={videoApi} frameRate={frameRate} />
  </Stack>
)
