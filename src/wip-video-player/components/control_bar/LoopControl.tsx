/** @jsxImportSource @emotion/react */

import { LoopIcon } from '../icons'
import React, { FC } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { ControlBarButton } from './ControlBarButton'
import { toggleLoop } from 'wip-video-player/actions'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'

type ExtendedControlBarProps = {
  videoApi: VideoApi
}

export const LoopControl: FC<ExtendedControlBarProps> = ({ videoApi }) => {
  useRerenderOnEvents(videoApi, ['isLooped'])

  return (
    <ControlBarButton
      onClick={() => {
        toggleLoop(videoApi)
      }}
    >
      {videoApi.getIsLooped() ? <LoopIcon /> : <LoopIcon />}
    </ControlBarButton>
  )
}
