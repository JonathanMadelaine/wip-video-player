/** @jsxImportSource @emotion/react */

import React, { FC, useState } from 'react'
import { VideoApi } from '../../wipVideoApi'
import { Block, Text } from 'baselift'
import { formatTime } from 'wip-video-player/utils'
import { useRerenderOnEvents } from 'wip-video-player/hooks/useRerenderOnEvents'

type TimeDisplayControlProps = {
  videoApi: VideoApi | undefined
  frameRate: number
}

export const TimeDisplayControl: FC<TimeDisplayControlProps> = ({ videoApi, frameRate }) => {
  // TODO: pull format from local storage
  const [format, setFormat] = useState<Parameters<typeof formatTime>[1]>('timecode')

  // TODO: hide duration on small screens

  useRerenderOnEvents(videoApi, ['duration', 'currentTime'])

  return (
    <Block
      css={{
        display: 'grid',
        gridTemplateColumns: '1fr auto 1fr',
        userSelect: 'none',
        letterSpacing: '1px',
        justifyContent: 'center',
        gap: '8px',
      }}
      onClick={() => {
        if (format === 'time') {
          setFormat('timecode')
        } else if (format === 'timecode') {
          setFormat('frames')
        } else {
          setFormat('time')
        }
      }}
    >
      <Text
        css={{
          flex: 1,
        }}
      >
        {formatTime({ timeInSeconds: videoApi?.getCurrentTime(), frameRate }, format)}
      </Text>
      <Text
        css={{
          color: 'var(--wvp-color-shade)',
        }}
      >
        {'/'}
      </Text>
      <Text
        css={{
          flex: 1,
          color: 'var(--wvp-color-shade)',
        }}
      >
        {formatTime({ timeInSeconds: videoApi?.getDuration(), frameRate }, format)}
      </Text>
    </Block>
  )
}
