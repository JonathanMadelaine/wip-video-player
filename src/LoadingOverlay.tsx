/** @jsxImportSource @emotion/react */

import { Block } from 'baselift'

export const LoadingOverlay = () => {
  return (
    <Block
      css={{
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        // TODO: use css animations to fade in loading state, but don't fade out, just cut straight to video
        // opacity: isLoading ? '1' : '0',
      }}
    >
      <Block
        css={{
          display: 'flex',
          blockSize: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          fontSize: '5rem',
          fontFamily: 'sans-serif',
        }}
      >
        <Block>{'LOADING'}</Block>
      </Block>
    </Block>
  )
}
