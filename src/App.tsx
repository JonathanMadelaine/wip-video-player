import { LoadingOverlay } from 'LoadingOverlay'
import { Player } from 'wip-video-player/components/Player'

import './wip-video-player/index.css'

export const App = () => {
  return (
    <>
      <style>
        {`body {
        font-family: sans-serif;
        background-color: #16191B;
      }`}
      </style>
      <Player
        media={{
          frameRate: 23.976,
          src: './test_hls.m3u8',
        }}
        loadingOverlay={<LoadingOverlay />}
      />
    </>
  )
}
